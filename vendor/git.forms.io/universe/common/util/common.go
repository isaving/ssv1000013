//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package util

import (
	"fmt"
	"github.com/go-errors/errors"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"time"
)

func IsInArray(array []string, iv string) bool {
	if 0 == len(array) {
		return false
	}

	for _, v := range array {
		if v == iv {
			return true
		}
	}

	return false
}

func IsArrayEqual(a, b []string) bool {
	// If one is nil, the other must also be nil.
	if (a == nil) != (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if !IsInArray(a, b[i]) {
			return false
		}
	}

	return true
}

func StringInList(key string, strList *[]string) bool {
	for _, k := range *strList {
		if k == key {
			return true
		}
	}
	return false
}

func Uint64InList(key uint64, list []uint64) bool {
	for _, v := range list {
		if key == v {
			return true
		}
	}
	return false
}

func EnvDefaultString(key, defaultValue string) string {
	v := os.Getenv(key)
	if v == "" {
		return defaultValue
	}
	return v
}

func EnvDefaultInt(key string, defaultValue int) int {
	v := os.Getenv(key)
	if v == "" {
		return defaultValue
	}
	n, err := strconv.Atoi(v)
	if err != nil {
		return defaultValue
	} else {
		return n
	}
}

func EnvSetString(key, defaultValue string) error {
	err := os.Setenv(key, defaultValue)
	if err != nil {
		return err
	}
	return nil
}

func getAscii() string {
	str := ""
	for i := 32; i <= 126; i++ {
		str += string(byte(i))
	}
	return str
}

var ascii = getAscii()

func RandomString(l int) string {
	bytes := []byte(ascii)
	result := make([]byte, 0)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

func ErrorToString(err error) string {
	var str string
	if err != nil {
		switch err.(type) {
		case *errors.Error:
			if nil != err.(*errors.Error) && nil != err.(*errors.Error).Err {
				str = err.(*errors.Error).ErrorStack()
			} else {
				var buf [2 << 10]byte
				stackFrame := string(buf[:runtime.Stack(buf[:], true)])
				str = fmt.Sprintf("Input Error instance invalid, stack frame=%s, please check,err=%++v", stackFrame, err)
			}
		default:
			str = err.Error()
		}
	} else {
		var buf [2 << 10]byte
		stackFrame := string(buf[:runtime.Stack(buf[:], true)])
		str = fmt.Sprintf("Input Error instance is nil, stack frame=%s, please check!", stackFrame)
	}

	return str
}

func IsError(e error, original error) bool {
	return errors.Is(e, original)
}
