package services

import (
	"git.forms.io/isaving/sv/ssv1000013/models"
	"testing"
)

var cu000005Response = `{"returnCode":"0","returnMsg":"success","data":{"Status":"ok"}}`

func (impl *Ssv1000013Impl) RequestSyncServiceElementKey(
	elementType, elementId, serviceKey string,
	requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "CU000005":
		responseData = []byte(cu000005Response)

	}

	return responseData, nil
}

func TestTryCommService(t *testing.T) {
	impl := &Ssv1000013Impl{
		Sv100013I: &models.SSV1000013I{
			CustId:        "1",
			Account:       "1",
			AccuntNme:     "3",
			CstmrCntctPh:  "3",
			CstmrCntctAdd: "3",
			CstmrCntctEm:  "3",
		},
	}

	sv100013O, err := impl.TrySsv1000013(impl.Sv100013I)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(sv100013O)
	}
}

func TestConfirmSsv0000004(t *testing.T) {
	impl := &Ssv1000013Impl{
		Sv100013I: &models.SSV1000013I{
			CustId:        "1",
			Account:       "1",
			AccuntNme:     "3",
			CstmrCntctPh:  "3",
			CstmrCntctAdd: "3",
			CstmrCntctEm:  "3",
		},
	}

	sv100013O, err := impl.ConfirmSsv1000013(impl.Sv100013I)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(sv100013O)
	}
}


func TestCancelSsv0000004(t *testing.T) {
	impl := &Ssv1000013Impl{
		Sv100013I: &models.SSV1000013I{
			CustId:        "1",
			Account:       "1",
			AccuntNme:     "3",
			CstmrCntctPh:  "3",
			CstmrCntctAdd: "3",
			CstmrCntctEm:  "3",
		},
	}

	sv100013O, err := impl.CancelSsv1000013(impl.Sv100013I)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(sv100013O)
	}
}

