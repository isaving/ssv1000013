//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/sv/ssv1000013/constant"
	"git.forms.io/isaving/sv/ssv1000013/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)

var Ssv1000013Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv1000013",
	ConfirmMethod: "ConfirmSsv1000013",
	CancelMethod:  "CancelSsv1000013",
}

type Ssv1000013 interface {
	TrySsv1000013(*models.SSV1000013I) (*models.SSV1000013O, error)
	ConfirmSsv1000013(*models.SSV1000013I) (*models.SSV1000013O, error)
	CancelSsv1000013(*models.SSV1000013I) (*models.SSV1000013O, error)
}

type Ssv1000013Impl struct {
	services.CommonTCCService

	Sv100013O *models.SSV1000013O
	Sv100013I *models.SSV1000013I
}

// @Desc Ssv1000013 process
// @Author
// @Date 2020-12-04
func (impl *Ssv1000013Impl) TrySsv1000013(ssv1000013I *models.SSV1000013I) (ssv1000013O *models.SSV1000013O, err error) {

	err = impl.updateSCU0000004()
	if err != nil {
		return nil, err
	}
	return ssv1000013O, nil
}

func (impl *Ssv1000013Impl) ConfirmSsv1000013(ssv1000013I *models.SSV1000013I) (ssv1000013O *models.SSV1000013O, err error) {
	log.Debug("Start confirm ssv1000013")
	return nil, nil
}

func (impl *Ssv1000013Impl) CancelSsv1000013(ssv1000013I *models.SSV1000013I) (ssv1000013O *models.SSV1000013O, err error) {
	log.Debug("Start cancel ssv1000013")
	return nil, nil
}

//CU000004
func (impl *Ssv1000013Impl) updateSCU0000004() error {
	scu0000004I := models.SCU0000004I{
		CustId:  impl.Sv100013I.CustId,
		Account: impl.Sv100013I.Account, //合约号
		AListContact: []models.AListContactArr{
			{
				UseType:         constant.CONSTNUM0,          //使用类别  默认传0
				Effectivestatus: constant.CONSTNUM1,          //有效标志
				ContactType:     constant.CONSTNUM04,         //邮箱
				Contact:         impl.Sv100013I.CstmrCntctEm, //邮箱号
			},
			{
				UseType:         constant.CONSTNUM0,          //使用类别  默认传0
				Effectivestatus: constant.CONSTNUM1,          //有效标志
				ContactType:     constant.CONSTNUM09,         //手机
				Contact:         impl.Sv100013I.CstmrCntctPh, //手机号
			},
		},
		AListAddr: []models.AListAddrArr{
			{
				UseType: constant.CONSTNUM0, //现对应的是--地址类型
				//AddrType: constant.CONSTNUM1,//家庭地址
				Address: impl.Sv100013I.CstmrCntctAdd,
			},
		},
	}
	if impl.Sv100013I.AccuntNme != "" {
		scu0000004I.ObjBasic.CustName = impl.Sv100013I.AccuntNme
	}

	requestBody, err := models.PackRequest(scu0000004I)
	if err != nil {
		return errors.Errorf(constant.ERRCODE_IL9, "%v", err)
	}
	_, err = impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constant.CU000004, requestBody)
	if err != nil {
		return errors.Errorf(constant.ERRCODE_IL9, "updateSCU0000004 failed : %v", err)
	}

	return nil
}
