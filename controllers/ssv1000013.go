//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/sv/ssv1000013/models"
	"git.forms.io/isaving/sv/ssv1000013/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000013Controller struct {
	controllers.CommTCCController
}

func (*Ssv1000013Controller) ControllerName() string {
	return "Ssv1000013Controller"
}

// @Desc ssv1000013 controller
// @Description Entry
// @Param ssv1000013 body models.SSV1000013I true "body for user content"
// @Success 200 {object} models.SSV1000013O
// @router /ssv1000013 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv1000013Controller) Ssv1000013() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000013Controller.Ssv1000013 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000013I := &models.SSV1000013I{}
	if err := models.UnPackRequest(c.Req.Body, ssv1000013I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv1000013I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000013 := &services.Ssv1000013Impl{}
	ssv1000013.New(c.CommTCCController)
	ssv1000013.Sv100013I = ssv1000013I
	//ssv1000013.DlsInterface = &commclient.DlsOperate{}
	ssv1000013Compensable := services.Ssv1000013Compensable

	proxy, err := aspect.NewDTSProxy(ssv1000013, ssv1000013Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv1000013I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv1000013O, ok := rsp.(*models.SSV1000013O); ok {
		if responseBody, err := models.PackResponse(ssv1000013O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv1000013 Controller
// @Description ssv1000013 controller
// @Param Ssv1000013 body models.SSV1000013I true body for SSV1000013 content
// @Success 200 {object} models.SSV1000013O
// @router /create [post]
/*func (c *Ssv1000013Controller) SWSsv1000013() {
	//Here is to generate API documentation, no need to implement methods
}
*/