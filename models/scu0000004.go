//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SCU0000004I struct {
	CustId   string      `validate:"required,max=20" description:"Customer Number"`    //客户号		Y
	Account  string      `validate:"required,max=30"description:"Type of certificate"` //合约号		N
	ObjBasic ObjBaseInfo `json:"ObjBasic,omitempty" description:"Type of certificate" `           //基本信息结构体	 Object
	//AListCertificate []AListCertificateArr `description:"Type of certificate"`                        //证件信息数组    array
	AListContact []AListContactArr `description:"Type of certificate"` //联系信息数组    array        合约号必输
	AListAddr    []AListAddrArr    `description:"Type of certificate"` //地址信息数组    array        合约号必输
	//ObjPublic        PublicInfoObj         `description:"Type of certificate"`                        //交易公共信息
}

//基本信息结构体	 Object
type ObjBaseInfo struct {
	CustName string
	//LastName  string `validate:"max=320" description:"First name"`   //姓氏
	//FirstName string `validate:"max=320" description:"Last name"`    //名称
	//Gender    string `validate:"max=1" description:"Gender"`         //性别
	//Birthdate string `validate:"max=10" description:"Birthday"`      //生日
	//Nation    string `validate:"max=2" description:"Nation"`         //民族
	//Marriage  string `validate:"max=2" description:"Marital status"` //婚姻情况
}

//证件信息数组    array
type AListCertificateArr struct {
	//RecordId     string `validate:"max=11" description:"ID"`                              //证件ID
	//IdType     string `validate:"max=4" description:"Type of certificate"`   //证件类型		N
	//IdNo       string `validate:"max=20" description:"ID number"`            //证件号码		N
	//DueDate    string `validate:"max=10" description:"Certificate due date"` //证件到期日	N
	//CertStatus string `validate:"max=3" description:"Document status"`       //证件状态		N	NOR-正常; OVD-过期; CAN-注销
	////OpenCertFlag string `validate:"max=1" description:"Account opening certificate sign"` //开户证件标志	N	0-否;1-是
	//IdNation string `validate:"max=2" description:"Issuing country"` //发证国家
	//UseType  string
}

//联系信息数组    array
type AListContactArr struct {
	//ContactId       string `validate:"max=32" description:"Contact information ID"`  //Y  联系信息ID
	Effectivestatus string `validate:"max=2" description:"Effective state"`          //有效状态		N	1-有效;9-无效
	ContactType     string `validate:"max=2" description:"Contact information type"` //联系信息类型
	Contact         string `validate:"max=320" description:"Contact details"`        //联系方式
	//Name            string `validate:"max=320" description:"Name"`                   //称谓
	UseType string
	//RegistFlag      string `validate:"max=1" description:"Registration Information Sign"` //注册信息标志(0-否;1-是)
}

//地址信息数组    array
type AListAddrArr struct {
	UseType string
	//ContactId   string `validate:"max=32" description:"Contact information ID"` //  联系信息ID   Y
	//AddrType    string `validate:"max=2" description:"Address type"`       //地址类型		Y	"1- 家庭地址 2- 办公地址 3- 证件地址 4- 注册地址 9- 其他地址 5- 户籍地址"
	//Nationality string `validate:"max=2" description:"Address country"`    //地址国家		N
	Address string `validate:"max=200" description:"Address"` //详细地址		N
	//Address1    string `validate:"max=200" description:"province / State"` //省/州		N
	//Address2    string `validate:"max=200" description:"City"`             //市		N
	//Address3    string `validate:"max=200" description:"District/County"`  //区/县		N
}

//交易公共信息
type PublicInfoObj struct {
	BranchNo string `description:"Institution number"` //机构编号 validate:"max=4"
	TellerId string `description:"Staff code"`         //员工编号 validate:"max=4"
}

type SCU0000004O struct {
}

// @Desc Build request message
func (o *SCU0000004I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SCU0000004I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SCU0000004O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SCU0000004O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SCU0000004I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SCU0000004I) GetServiceKey() string {
	return "scu0000004"
}
