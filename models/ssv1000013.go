//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000013I struct {
	CustId      string `validate:"required" description:"Client number"` //客户号
	Account     string `validate:"required" description:"Contract ID"`   //合约号

	AccuntNme     string               `validate:"max=120" description:"Account name"`       //账户名称
	CstmrCntctPh  string               `validate:"max=20" description:"Mobile phone number"` //手机号码
	CstmrCntctAdd string               `validate:"max=200" description:"Contact address"`    //联系地址
	CstmrCntctEm  string               `validate:"max=200" description:"Mailbox"`            //邮箱

	//ObjPublic     SSV1000013IObjPublic ` description:"Transaction public information"`       //交易公共信息
}
/*type SSV1000013IObjPublic struct {
	BranchNo string
	TellerId string
}*/

/*//账户名称  手机号码  联系地址  邮箱
type AListContactArr struct {
	ContactId   string `description:"Contact information ID"`   //联系信息ID
	ContactType string `description:"Contact information type"` //联系信息类型   //01-家庭电话 02-办公电话 03-手机（目前为钱包专用） 04-EMAIL 05-传真 06-微信 07-QQ 08-传真 09-手机 99-其他
	Contact     string `description:"Contact information"`      //联系方式
	Name        string `description:"Name"`                     //称谓
}
type AListAddr struct {
	//ContactId  string `description:"Contact information ID"` //联系信息ID
	AddrType   string `description:"Address category" `      //地址类别
	NationCode string `description:"Country"`                //国别
	Province   string `description:"Province / State" `      //省/州
	City       string `description:"City"`                   //城市
	District   string `description:"District"`               //区县
	Addr       string `description:"Address"`                //详细地址
}
*/type SSV1000013O struct {
}

// @Desc Build request message
func (o *SSV1000013I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000013I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000013O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000013O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000013I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000013I) GetServiceKey() string {
	return "ssv1000013"
}
