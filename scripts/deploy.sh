#!/bin/bash

[ -d deploy ] && {
    rm -rf deploy/*
} || {
    mkdir deploy
}

[ $# -ne 2 ] && {
	path=$(
		cd "$(dirname "$0")"
		pwd
	)
	project=$(echo ${path##*/}|awk '{print tolower($0)}')
	group=$(echo $path | rev | cut -d '/' -f 2 | rev)
} || {
	group=$1
	project=$(echo $2|awk '{print tolower($0)}')
}

[ -d deploy/$group ] || mkdir deploy/$group
cat <<EOF >deploy/$group/package.json
{
	"name":"$group",
	"description":"$group public chart template",
	"version":"0.0.1",
	"namespace": "default",
	"type":"legosvr",
	"permission": "public",
	"resource": {
		"cpu": 2,
		"memory": "16Gi",
		"disk": "20Gi"
	},
	"applications":[
		{
			"version":"0.0.1",
			"apolloAppId":"",
			"index":1,
			"necessary":true,
			"name": "$project"
		}
	],
	"images":[],
	"ports":[],
	"dependencies":[]
}
EOF

echo "create deploy/$group/package.json success"

mkdir deploy/$group/$project

cat <<EOF >deploy/$group/$project/values.yaml
## The image repository
image: "$group/$project"

## The  image tag
imageTag: "v0.0.1"  # Confluent image for Kafka 2.0.0

commImage: "hub.universe.com/universe/comm-agent"
commImageTag: "v0.2.5.6"

## Specify a imagePullPolicy
imagePullPolicy: "IfNotPresent"

## The StatefulSet installs 3 pods by default
minInstNum: 1

## Load dls configuration, default num 0 don't load ,num 1 load, old config num 9.
dlsFlag: 1

## uid
uid: 1000

## system group id
gid: 1000
## serviceAccount
serviceAccount: universe
## commAgentShareDir: "/data/app/secret"
commAgentShareDir: "/data/comm/conf"
## appShareDir: "/data/comm/secret"
appShareDir: "/data/app/conf"
topicInfo: "[]"

EOF

echo "create deploy/$group/$project/values.yaml success"
confirm=$(cat conf/app.conf|grep topic.confirmName| awk -F= '{print $2}'|sed 's/ //g')
cancel=$(cat conf/app.conf|grep topic.cancelName| awk -F= '{print $2}'|sed 's/ //g')
cat <<EOF >deploy/$group/$project/data.json
[
	{
		"section": "Config",
		"upgradable": true,
		"keys": [
		]
	}
]
EOF
echo "create deploy/$group/$project/data.json success"
cat <<EOF >deploy/$group/$project/Chart.yaml
name: $project
version: 0.0.1
appVersion: 0.0.1
description: A Helm chart for Kubernetes
keywords:
- engine
home: http://intellif.io/
icon: http://pmo4b1dce.pic37.websiteonline.cn/upload/if_web_logo_wu2d.png
maintainers:
- name: intellif
  email: ci@intellif.com
engine: gotpl
EOF
echo "create deploy/$group/$project/Chart.yaml success"
[ -d deploy/$group/$project/templates ] || mkdir deploy/$group/$project/templates
[ -f deploymountfile.tmp ] && rm deploymountfile.tmp
[ -f deploymountvolumkey.tmp ] && rm deploymountvolumkey.tmp
for i in $(ls conf | grep -v app.conf); do
	cat <<EOF >>deploymountfile.tmp
            - name: config-app
              mountPath: /data/app/conf/$i
              subPath: $i
EOF
	cat <<EOF >>deploymountvolumkey.tmp
              - key: $i
                path: $i
EOF
done

cat <<EOF >deploy/$group/$project/templates/deploy.yaml
apiVersion: apps/v1beta2
kind: StatefulSet
metadata:
  name: {{ .Values.serviceName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.serviceName }}
    chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
    release: "{{ .Release.Name }}"
    heritage: "{{ .Release.Service }}"
spec:
  serviceName: {{ .Values.serviceName }}
  selector:
    matchLabels:
      app: {{ .Values.serviceName }}
  replicas: {{ .Values.minInstNum}}
  template:
    metadata:
      labels:
        app: {{ .Values.serviceName }}
    spec:
      serviceAccountName: {{ .Values.serviceAccount }}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                    - {{ .Values.serviceName }}
              topologyKey: "kubernetes.io/hostname"
      {{- if or .Values.commAgentShareDir .Values.appShareDir }}
      initContainers:
        - name: init
          image: 'hub.universe.com/library/alpine:latest'
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - mountPath: /data/app/conf/app.conf
              name: config-app
              subPath: app.conf
$([ -s deploymountfile.tmp ] && cat deploymountfile.tmp)
            - mountPath: /data/comm/app.conf
              name: config-comm
              subPath: app.conf
            - mountPath: /data/comm/kms_pub.pem
              name: config-comm
              subPath: kms_pub.pem
            - mountPath: /data/comm/mq_session.json
              name: config-comm
              subPath: mq_session.json
            {{- if .Values.commAgentShareDir }}
            - mountPath: /opt/comm
              name: comm-share-dir
            {{- end }}
            {{- if .Values.appShareDir }}
            - mountPath: /opt/app
              name: app-share-dir
            {{- end }}
            - name: commlog-path
              mountPath: /data/commlogs
            - name: log-path
              mountPath: /data/logs
          command:
            - 'sh'
            - '-xc'
            - 'if [ -d /opt/app ];then cp -rf /data/app/conf/* /opt/app; fi && if [ -d /opt/comm ];then cp -rf /data/comm/* /opt/comm; fi && chown -R 1000:1000 /opt/comm && chown -R 1000:1000 /opt/app && chown -R 1000:1000 /data/commlogs && chown -R 1000:1000 /data/logs'
      {{- end }}
      containers:
        - name: comm-agent
          image: "{{ .Values.commImage }}:{{ .Values.commImageTag }}"
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          securityContext:
            allowPrivilegeEscalation: false
          volumeMounts:
            {{- if .Values.commAgentShareDir }}
            - name: comm-share-dir
              mountPath: {{ .Values.commAgentShareDir }}
            {{- else }}
            - name: config-comm
              mountPath: /data/comm/conf/app.conf
              subPath: app.conf
            - name: config-comm
              mountPath: /data/comm/conf/mq_session.json
              subPath: mq_session.json
            - mountPath: /data/comm/conf/kms_pub.pem
              name: config-comm
              subPath: kms_pub.pem
            {{- end }}
            - name: host-time
              mountPath: /etc/localtime
              readOnly: true
            - name: commlog-path
              mountPath: /data/commlogs
          resources:
            limits:
              cpu: 1400m
              memory: 1Gi
            requests:
              cpu: "0"
              memory: "0"
          env:
            - name: NODE_ID
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: INSTANCE_ID
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
        - name: {{ .Values.serviceName }}
          image: "hub.universe.com/{{ .Values.image }}:{{ .Values.imageTag }}"
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          securityContext:
            allowPrivilegeEscalation: false
          volumeMounts:
            {{- if .Values.appShareDir }}
            - name: app-share-dir
              mountPath: {{ .Values.appShareDir }}
            {{- else }}
            - name: config-app
              mountPath: /data/app/conf/app.conf
              subPath: app.conf
$([ -s deploymountfile.tmp ] && cat deploymountfile.tmp)
            {{- end }}
            - name: host-time
              mountPath: /etc/localtime
              readOnly: true
            - name: log-path
              mountPath: /data/logs
          resources:
            limits:
              cpu: 1200m
              memory: 4Gi
            requests:
              cpu: "0"
              memory: "0"
          env:
            - name: NODE_ID
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: INSTANCE_ID
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            {{- range \$key, \$value := .Values.env }}
            - name: "{{ \$key }}"
              value: "{{ \$value }}"
            {{- end }}
      volumes:
        - name: config-comm
          configMap:
            name: "{{ .Values.serviceName }}-agentconfig"
            items:
              - key: app.conf
                path: app.conf
              - key: kms_pub.pem
                path: kms_pub.pem
              - key: mq_session.json
                path: mq_session.json
        - name: config-app
          configMap:
            name: "{{ .Values.serviceName }}-config"
            items:
              - key: app.conf
                path: app.conf
$([ -s deploymountvolumkey.tmp ] && cat deploymountvolumkey.tmp)
        - name: host-time
          hostPath:
            path: /etc/localtime
        - name: service-relation
          hostPath:
            path: /data/service_relation/service_relation.conf
        - name: log-path
          hostPath:
            path: "/data/logs/{{ .Values.serviceName }}"
        - name: commlog-path
          hostPath:
            path: "/data/commlogs/{{ .Values.serviceName }}"
        {{- if .Values.appShareDir }}
        - name: app-share-dir
          emptyDir: {}
        {{- end }}
        {{- if .Values.commAgentShareDir }}
        - name: comm-share-dir
          emptyDir: {}
        {{- end }}
EOF
echo "create deploy/$group/$project/templates/deploy.yaml success"
cat <<EOF >deploy/$group/$project/templates/comm.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ .Values.serviceName }}-agentconfig"
  namespace: {{ .Values.namespace }}
data:
  app.conf: |
    appname = comm-agent
    httpaddr = "127.0.0.1"
    httpport = 18080
    ;runmode = dev
    autorender = false
    copyrequestbody = true

    [log]
    logLevel = info
    logFile  = comm-agent.log

    [client]
    callbackUrl=http://127.0.0.1:18082/v1/newmsg

    [mq_session]
    configFilePath = ./conf/mq_session.json

    [app]
    session_name = default
    org = {{ .Values.org }}
    az = {{ .Values.az }}
    dcn = {{ .Values.dcnInstance }}
    serviceId = {{ .Values.serviceId }}
    name = comm-agent
    dlsflag = {{ .Values.dlsFlag }}
    enableClientHealthStatCheck=false
    persistentDeliveryMode=false
    enableAppendGlobalTxnIdToTopic=false
    exclusionsAppendGlobalTxnIdTopics=rapm000;DlscmAgentConf;KmsGetKeysWithCrypto;KmsGetRotateKeysWithCrypto;KmsGetKeys

    [dls_query]
    agent_config=DlscmAgentConf

    [deployment]
    enableSecure=false
    mode="C"

    [lookup]
    enable = true
    commonDcn = {{ .Values.dcn }}
    dasPrefix = DA

    [apm]
    enable=true

    [crypto]
    enable = false
    algo = AES256
    mode = GCM
    padding = "PKCS7"
    upstreamServices =
    exclusionsTopics = rapm000;DlscmAgentConf;KmsGetKeysWithCrypto;KmsGetRotateKeysWithCrypto;KmsGetKeys;DTS_AGENT_REGISTER;DTS_AGENT_ENLIST;DTS_AGENT_TRY_RESULT_REPORT
    getServicesKeysTopic = KmsGetKeysWithCrypto
    rotateServicesKeysTopic = KmsGetRotateKeysWithCrypto
    serviceKeyActiveHours = 48
    keyGenerationTimeInAdvance = 24
  kms_pub.pem: |
    -----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx1ktmhysVYIhvA3VBSEl
    iml+jLgNkO7GVXE4Ha5tOgNnVM893tpJ7dS0eV7NQSP9BwGqGSAxNXqfhtvRH2D1
    ROUXhrPeiy2Ecp1MK1JS/Cp1GhbCOZxV9Kp6A6bInTxktoG8WuW+bjhSDO3vqr8e
    RNyzB2MvcnxLzE3V/LSP5+0UIp0M+kPN+XJCxJo4En3UE6fTxvjKIxuj8bNs61OZ
    B5lZr91Qn0XfxoZcfhnTCGE7QyZDKVkcoYvapXPjyVtHpw9OL5057+dceOstzDV8
    KfDZERNNc7hD/PVQXFOBpwsv8XVm90ESvQtYhKcrOZjMzJyPVUms9Xs/5ULLXcwR
    twIDAQAB
    -----END PUBLIC KEY-----
  mq_session.json: |
    [
      {
        "driver": "solace",
        "name": "default",
        "attributes": {
            "SESSION_VPN_NAME": "{{ .Values.vpnName }}",
            "SESSION_USERNAME": "{{ .Values.vpnUsername }}",
            "SESSION_PASSWORD": "{{ .Values.vpnPassword }}",
            "SESSION_HOST": "tcp:{{ .Values.vpnVip }}"
        },
        "topics": {{ .Values.topicInfo }},
        "queues": {{ .Values.queueInfo }}
      }
    ]
EOF
echo "create deploy/$group/$project/templates/comm.yaml success"
[ -f $group.$project.tmp ] && rm $group.$project.tmp

for i in $(ls conf); do
	echo "  "$i: \|+ >>configmap.tmp
	cat conf/$i |
	  sed "s/appname.*/appname = {{ .Values.serviceName }}/g" |
	  sed "s/^runmode.*/;runmode = dev/g" |
	  sed "s/callback_port.*/callback_port = 18082/g" |
	  sed "s/comm_agent_address.*/comm_agent_address = http:\/\/127.0.0.1:18080/g" |
	  sed "s/logFileRootPath.*/logFileRootPath=\/data\/logs/g" |
	  sed "s/logFile\ =.*/logFile = {{ .Values.serviceName }}.log/g" |
	  sed "s/logLevel\ =.*/logLevel = info/g" |
	  sed "s/logLevel=.*/logLevel = info/g" |
	  sed "s/intervalSeconds.*/intervalSeconds=30/g" |
	  sed "s/logLevelUnixSocket.*/logLevelUnixSocket=\/tmp\/{{ .Values.serviceName }}.sock/g" |

	  sed "s/addr \=.*/addr \= {{ .Values.mysqlVip}}:{{ .Values.mysqlPort}} /g" |
	  sed "s/user \=.*/user \= {{ .Values.mysqlUsername }}/g" |
	  sed "s/password \=.*/password \= {{ .Values.mysqlPassword}} /g" |
	  sed "s/database \=.*/database \= {{ .Values.mysqlDbName }} /g" |

	  sed "s/organization.*/organization ={{ .Values.org }}/g" |
	  sed "s/nodeId.*/nodeId = \$NODE_ID/g" |
	  sed "s/groupDcn.*/groupDcn ={{ .Values.dcn }}/g" |
	  sed "s/commonDcn.*/commonDcn ={{ .Values.dcn }}/g" |
	  sed "s/dataCenterNode.*/dataCenterNode ={{ .Values.dcnInstance }}/g" |
	  sed "s/topic.confirmName.*/topic.confirmName ={{ .Values.dtsConfirmTopic }}/g" |
	  sed "s/topic.cancelName.*/topic.cancelName ={{ .Values.dtsCancelTopic }}/g" |
	  sed "s/^/    /g" >>configmap.tmp
	echo "" >>configmap.tmp
	echo "" >>configmap.tmp
done

cat <<EOF >deploy/$group/$project/templates/app.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ .Values.serviceName }}-config"
  namespace: {{ .Values.namespace }}
data:
$(cat configmap.tmp)

EOF
echo "create deploy/$group/$project/templates/app.yaml success"

rm configmap.tmp
rm deploymountfile.tmp
rm deploymountvolumkey.tmp
